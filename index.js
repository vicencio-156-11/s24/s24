// why mongoDB's logo is a leaf because it describes natural and simple

// Node.js -> will provide us with a runtime environment (RTE) that will allow us to execute our program.

// RTE (Runtime Environment) -> is the environment or system in which a program can be executed.


// TASK: Let's cretae a standard server setup using plain NodeJS

// Identify and prepare the components/ingredients needed in order to execute the task.
// http (hypertext transfer protocol)
// http is a built in module of NodeJS that will alow us to esatblish a connection and allow us to transfer data over the HTTP.
// use a funtion called require() in order to acquire the desired module. Repackage the module and store it inside a new variable.


let http = require('http');

	//http will provide us with all the components needed to estabish a server.
	
	// acquire http using dot (.) notation

	// createServer() will allow us to create HTTP server.

	// provide or instruct the server what to do.
	// print a simple message.
	// request: client to server
	// response: server to client

	// Identify and decribe a location where the connection will happen, in order to provide a proper medium for both parties, and bind the connection to the desired port number.
	// Identify a point where we want to terminate the transmission using the end() function
	// ctrl-c to terminate


let port = 3000;

// listen() is use to bind and listen to a specific port whenever its being accessed by your computer

http.createServer(function(request , response){
	response.write('Hello Batch 156!');
	response.end(); // terminate the transmission
}).listen(port);

// create a response in the terminal to confirm if a connection has been successfully establish.
// 1. we can give a confirmation to the client that a connection is working
// 2. we can specify what address/location the connection was establish.
// we will use template literals in order to utilize placeholders

// console.log('Hello Batch 156!');
console.log(`Server is running with nodemon on port: ${port}`);


// What can we do for us to be able to automatically hotfix the system without restarting a serve?

	// 1. Initialize a npm into your local project.
		// syntax: npm init (into the project)
		// shortcut  syntax: npm init -y (yes)
	
		// package.json -> "the Heart of any Node Projects", it records all the important metadat about the projects, (libraries, packages, functions) that makes up the system
	// 2. Install  'nodemon' into our project
		// PRACTICE SKILL"
			// install:
				// npm install <dependency>
				// npm i <dependency>

			// uninstall :
				// npm uninstall <dependency>
				// npm un <dependency>


			// installing multiple dependencies:
				// npm i <list of dependencies>
					// express

	// 3. run your app using the nodemon utility engine
		// identify entry point (app.js)
		// note: make sure that nodemon is recognized by your file system structure
		
		//npm install -g nodemon (global)
			// the library is bein installed  "globally" into your machine's file system so that the program will be recognizable accross your file system
			// (*YOU ONLY HAVE TO EXECUTE THIS COMMAND ONCE.*)
		
		// nodemon app.js

		// nodemon utility ->is a CLI utility tool, it's task is to wrap your node js and watch for any cahnges in the file system and automatically restart/hotfic the process

		// create your personalized script to execute the app using the nodemon utility
			// register a new command to start the app

			// start -> common execute script for node apps.


			// others -> run command to identify or recognize the uncommon script

// [SEGWAY] Register sublime Text 3 as part of your environment variables.

// 1. Locate in your PC where Sublime Text 3 is installed.
// 2. Copy the path where sublime text 3 is installed
	// C:\Program Files\Sublime Text

// 3. Locate the environment variables in your system
// Settings>System>About>Advanced system settings>System properties> Environment variables > System variables> Double click Path > Under Edit Variable, click New > Paste location in Program Files > Okay > Okay